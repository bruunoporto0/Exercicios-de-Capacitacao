
/* Modelo de Código para Controle de Carro c/ Arduino */
 
// Definir os pinos de utilização do Driver L298.
// IN1 e IN2 sao para a roda da direita
// IN3 e IN4 sao para a roda da esquerda
//
//
const int IN1 = 1;
const int IN2 = 2;
const int IN3 = 3;
const int IN4 = 4;
// Variáveis Úteis
int state_temp;
int vSpeed = 200;   // Define velocidade padrão 0 - 255.
char state;

void setup() {
  // Inicializar as portas como entrada e saída.
  //
  //
  //
  //
pinMode(IN1, OUTPUT);
pinMode(IN2, OUTPUT);
pinMode(IN3, OUTPUT);
pinMode(IN4, OUTPUT);
  // Inicializar a comunicação serial.
  //
 Serial.begin(9600);
}
 
void loop() {
 
  // Salva os valores da variável 'state'
  if (Serial.available() > 0) {
    state_temp = Serial.read();
    state = state_temp;
  }
 
  // Se o estado recebido for igual a '8', o carro se movimenta para frente.
  if (state == '8') {
    Serial.println("Comando para Frente");
    // Comandos de Motores
    //
     analogWrite(IN1, vSpeed);
     digitalWrite(IN2, LOW);
     analogWrite(IN3, vSpeed);
     digitalWrite(IN4, LOW);
    
    
  }
    // Se o estado recebido for igual a '7', o carro se movimenta para Frente Esquerda.
    else if (state == '7') {  
      Serial.println("Comando para Frente-Esquerda");
      // Comandos de Motores
     analogWrite(IN1, vSpeed);
     digitalWrite(IN2, LOW);
     analogWrite(IN3, 150);
     digitalWrite(IN4, LOW);
  }
    // Se o estado recebido for igual a '9', o carro se movimenta para Frente Direita.
    else if (state == '9') {   
    Serial.println("Comando para Frente-Direita");
      // Comandos de Motores
     analogWrite(IN1, 150);
     digitalWrite(IN2, LOW);
     analogWrite(IN3, vSpeed);
     digitalWrite(IN4, LOW);
  }
  // Se o estado recebido for igual a '2', o carro se movimenta para trás.
  else if (state == '2') { 
    Serial.println("Comando para Trás");
      // Comandos de Motores
     digitalWrite(IN1, LOW);
     analogWrite(IN2, vSpeed);
     digitalWrite(IN3, LOW);
     analogWrite(IN4, vSpeed);
     
  }
   // Se o estado recebido for igual a '1', o carro se movimenta para Trás Esquerda.
   else if (state == '1') {  
    Serial.println("Comando para Trás-Esquerda");
      // Comandos de Motores
      digitalWrite(IN1, LOW);
     analogWrite(IN2, vSpeed);
     digitalWrite(IN3, LOW);
     analogWrite(IN4, 150);
  }
      //
  
  // Se o estado recebido for igual a '3', o carro se movimenta para Trás Direita.
  else if (state == '3') {  
    Serial.println("Comando para Trás-Direita");
      // Comandos de Motores
        digitalWrite(IN1, LOW);
        analogWrite(IN2, 150);
        digitalWrite(IN3, LOW);
        analogWrite(IN4, vSpeed); 
  }
  // Se o estado recebido for igual a '4', o carro se movimenta para esquerda.
  else if (state == '4') {   
    Serial.print("Comando para Esquerda");
      // Comandos de Motores
        analogWrite(IN1, vSpeed);
        digitalWrite(IN2, LOW);
        digitalWrite(IN3, LOW);
        digitalWrite(IN4, LOW);

  }
  // Se o estado recebido for igual a '6', o carro se movimenta para direita.
  else if (state == '6') {   
    Serial.println("Comando para Direita");
      // Comandos de Motores
        digitalWrite(IN1, LOW);
        digitalWrite(IN2, LOW);
        analogWrite(IN3, vSpeed);
        digitalWrite(IN4, LOW);

      //
      //
      //
  }
  // Se o estado recebido for igual a '5', o carro permanece parado.
  else if (state == '5') {   
    Serial.print("Comando para Parar");
      // Comandos de Motores
        digitalWrite(IN1, LOW);
        digitalWrite(IN2, LOW);
        digitalWrite(IN3, LOW);
        digitalWrite(IN4, LOW);

      //
      //
      //
  }
}

#!/usr/bin/python
import rospy
import tf2_ros
import tf2_msgs
import geometry_msgs.msg
import math

class translacao:
    def __init__(self, nome, ratio, main_body):
        self.nome = nome
        self.ratio = ratio
        self.tf = geometry_msgs.msg.TransformStamped()
        self.tf.header.frame_id = main_body
        self.tf.child_frame_id = nome
        self.tf.transform.translation.x = 0
        self.tf.transform.translation.y = 0
        self.tf.transform.translation.z = 0
        self.tf.transform.rotation.x = 0
        self.tf.transform.rotation.y = 0
        self.tf.transform.rotation.z = 0
        self.tf.transform.rotation.w = 1
    def __frame_update__(self):
        t = 2*rospy.Time.now().to_sec()* math.pi/(20*self.ratio**2)
        self.tf.transform.translation.x =self.ratio* math.sin(t)
        self.tf.transform.translation.y =self.ratio* math.cos(t)
        return self.tf

rospy.init_node("sistemasolar", anonymous="True")
rate = rospy.Rate(10)

r = rospy.get_param("raio")


broadcaster = tf2_ros.TransformBroadcaster()
planetas=[translacao("Hermes", r[0], "HELIO"), translacao("Afrodite", r[1], "HELIO"), translacao("Gaia", r[2], "HELIO"),translacao("Selene", r[3], "Gaia"), translacao("Ares", r[4] ,"HELIO"),translacao("Zeus", r[5],"HELIO"), translacao("Cronos", r[6], "HELIO"),translacao("Uranus", r[7], "HELIO"), translacao("Poseidon", r[8],"HELIO")]


while not rospy.is_shutdown():
        for i in planetas:
            broadcaster.sendTransform(i.__frame_update__())
rate.sleep()

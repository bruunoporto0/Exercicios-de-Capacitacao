# Nautilus exercises #

Here you'll find all nautilus's works done by Bruno Porto.

# Problem 1 #

You will find it in "euler_problem" directory. Open terminal and execute the command "python python.py".


# Problem 2 #

You will find it in "euler_problem" directory. Open terminal and execute the command "python problem2.py".

# Object Orientation #

to run this code you only need to open terminal and execute the command "python Orientacao_objeto.py".

# TF problem #

You will find it in "sistemasolar" directory (that is also the package of ROS) then open the terminal and run the commands "roscore", "roslaunch sistemasolar sis.launch" then "python sistema.py" and finally "rviz" to see the project.

# Publisher and Subscriber #

You will find it in "talker_and_listener" directory. Open terminal run "roscore", "talker.py" and "listener.py".

# Claw model #

You will find it in "modelo_de_garra" directory. Open gazebo and import model.sdf to visualize it.

# Arduino project #
 
You will find it in "Modelo_Entregavel" directory. just open the Modelo_Entregavel.ino file with arduino ide.

#  Sensors search #
 
Just open pesquisa_de_sensores.pdf file to analyze it.

# Finding nemo work #

You will find it in "segmentacao_de_cores" directory. Open the terminal and run "python visao.py".
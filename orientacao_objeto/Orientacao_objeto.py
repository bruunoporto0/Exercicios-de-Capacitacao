#orientacao a objetos
#campeonato de futebol espacial
class times:
    def __init__(self, nome_time, pontos, habilidade, descricao, casa_do_time, fama):
        self.nome_time =nome_time
        self.pontos = pontos
        self.habilide = habilidade
        self.descricao = descricao
        self.casa_do_time = casa_do_time
        self.fama = fama
    def __repr__(self):
        return "\n\n\n       INFORMACOES DOS TIMES     \n\n\n Nome do time: {}\n Sua pontuacao: {} \n Nivel de habilidade: {}\n Curiosidade sobre o time: {}\n De onde vem: {}\n Nivel de fama: {}".format(self.nome_time,self.pontos,self.habilide,self.descricao,self.casa_do_time,self.fama)
    def __add__(self, other):
        return self.pontos + other.pontos
    def __sub__ (self, other):
        return self.pontos - other.pontos
    def __gt__(self, other):
        return self.pontos > other.pontos
    
        
time1 = times("Os Pica Das Galaxias", 9999999, "muito acima da media", "jogam desde a existencia do universo","galaxia IC 1101","time mais aclamado nos jogos")
time2 = times("Os Marcianos", 3983049, "um pouco acima da media", "jogam desde que conheceram os egipcios", "marte", "conhecidos somente na via lactea")
time3 = times("Famosinhos Da Terra", 3868134, "Um pouco acima da media", "Time formado logo apos o e.t. bilu conhecer o e.t. de Varginha", "Qualquer lugar do espaco", "Conhecido apenas na terra")
time4 = times("Ta Na Hora De Virar Heroi", 10,"Muito inferior da media", "Time composto pelo ben 10 e seus aliens", "Terra", "Conhecido desde que ganhou a copatoon")
time5 = times("Space Jam Soccer Club", 2395899, "Na media", "Time Ficou famoso logo apos sua vitoria contra Monstars no campo de basquete","Terra da dimensao 33972", "Conhecidos em pelo menos 4 dimensoes")
Rank = [time3,time2,time1,time4,time5]


print("================================================================================")
print( "                       SOMA E SUBTRACAO DE PONTOS\n")
print("time1 + time2 = {}".format(time1+time2)) 
print("time5 - time3 = {}".format(time5-time3))
print("time2 + time4 = {}".format(time2+time4))
print("================================================================================")
print("                         RANK DOS TIMES")
Rank.sort()
print(Rank)
#!usr/bin/python
import cv2   #importar a biblioteca opencv

nemo=cv2.VideoCapture('./nemo.mp4')     #pegar o video do nemo
light_orange,dark_orange = (1,190, 200),(18,255,255)     #definindo o valor das cores
light_white, dark_white = (0, 0, 200),(145, 60, 255)
while (True):
    
    __,frame = nemo.read()
    frame= cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #alterando a imagem de BGR para HSV
    orangeMask = cv2.inRange(frame, light_orange, dark_orange) #definindo o range das cores
    whiteMask = cv2.inRange(frame, light_white, dark_white) #definindo o range das cores
    frame = cv2.bitwise_and(frame, frame, mask=orangeMask+whiteMask) #segmentacao das cores
    frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR) #alterando a imagem de HSV para BGR
    cv2.imshow("Frame",frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
frame.release()
cv2.destroyAllWindows()